import unittest
#import client
from server-new import Server #TODO: zmien sobie import!
from client-new import Client
import os


class ServerTest(unittest.TestCase):

    def test_instance_creation(self):
        x=Server()
        self.assertIsInstance(x,Server,"x jest instancja klasy Server?")

    def test_is_server_initialized(self):
        x=Server()
        port=1234  # cokolwiek, lepiej wyzsze niz nizsze ;-)
        self.assertGreater(x.init_server(port).fileno(), 0, "Server zainicjalizowany?")
        '''taki myk, jak cokolwiek w Linuksie sie uruchomi i nasluchuje, to dostaje deskryptor (numer socketu),
           ktory jest nieujemna liczba calkowita, takze mozna sprawdzac stan w ten sposob
        '''

    def test_are_functions_registered(self):
        x=Server()
        port=54321
        x.init_server(port)
        self.assertTrue(x.register_functions())


    def test_load_ports(self):
        x=Server()
        f=open("konfiguracja_serwera","w")
        f.seek(0)
        f.write(str(65))
        f.write("\n")
        f.close()
        x.load_ports()
        self.assertEqual(x.ports[-1],65)

    def test_save_config(self):
        x=Server()
        self.assertGreater(x.save_config(), 0)

    def test_got_ports(self):
        x=Server()
        x.get_ports()
        self.assertGreater(len(x.ports),0)


class ClientTest(unittest.TestCase):
    def test_instance_creation(self):
        y=Client()
        self.assertIsInstance(y,Client,"x jest instancja klasy Client?")

    def test_makedir(self):
        y=Client()
        os.rmdir("program") #gwarantujemy brak katalogu
        self.assertTrue(y.makedir())

    def test_load_config(self):
        y=Client()
        y.get_conf()
        self.assertGreater(len(y.port),0)
        self.assertGreater(len(y.IP),0)

    def test_save_config(self):
        y=Client()
        self.assertTrue(y.save_conf())

    def test_get_config(self):
        y=Client()
        self.assertGreater(y.port,0)
        self.assertGreater(y.IP,0)


if __name__ == '__main__':
    unittest.main()
