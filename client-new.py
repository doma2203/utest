#!/usr/bin/python
import xmlrpclib
import os
import shlex
import subprocess
import threading
import datetime
import filecmp
import glob
import RestrictedPython
from RestrictedPython import compile_restricted
from RestrictedPython.Guards import safe_builtins


class Client(object):
    def __init__(self):
        self.wybor=0
        self.port=list()
        self.IP=list()
        self.adres=list()
        self.result=None
        self.proxy=None
        self.name=None

    def save_conf(self):
        print '''Czy chcesz zapisac jako domyslna konfiguracje?
            1 - TAK
            2 - NIE'''
        self.wybor = int(raw_input())
        if self.wybor == 1:
            fil = open("konfiguracja_klienta_PORTY", "w")
            for a in self.port:
                fil.write(a)
                fil.write("\n")
            fil.close()
            fil = open("konfiguracja_klienta_IP", "w")
            fil.writelines(self.IP)
            fil.close()
            self.port = map(int, self.port)

    def load_conf(self):
        fil = open("konfiguracja_klienta_IP", "r")
        self.IP = fil.readlines()
        fil.close()
        fil = open("konfiguracja_klienta_PORTY", "r")
        port2 = fil.readlines()
        fil.close()
        for c in range(len(port2)):
            self.port.append(port2[c].rstrip())
        self.port = map(int, self.port)

    def get_conf(self):
        print "Wpisz adres IP serwera"
        self.IP = raw_input().split()
        print "Wpisz porty oddzielajac spacjami"
        self.port = raw_input().split()
        self.wybor = 0


    def makedir(self):
        if not os.path.isdir("program"):
            os.system("mkdir program")

    def main_menu(self):
        print "Witaj w programie klienta"
        while self.wybor != 1 and self.wybor != 2:
            print '''Aby uruchomic klienta, wybierz jeden z ponizszych krokow:
                  1 - Wpisz adres i porty na ktorych na ktorych ma byc uruchomiony
                  2 - Wczytaj skonfigurowane adres i porty '''
            self.wybor = int(raw_input())
        if self.wybor == 1:
            self.get_conf()
            while self.wybor != 1 and self.wybor != 2:
                self.save_conf()
        else:
            self.load_conf()
        self.makedir()



    def get_program(self):
        self.name = "program/p" + unicode(datetime.datetime.now().time()) + ".py"
        handle = open(self.name, "w")
        for x in self.port:
            self.adres.append(self.IP[0].rstrip() + ":" + str(x) + "/")
        for g in self.adres:
            self.proxy = xmlrpclib.ServerProxy(g)
            try:
                handle.write(proxy.send_program().data)
                break
            except:
                print ("Adres %s jest niedostepny" % g)
        handle.close()

    def compile_program(self):
        os.system('sudo chmod -R 777 program/')
        if (os.system('python -m compileall -l %s' % self.name)) == 0:
            self.proxy.get_compile_result("true")
        else:
            self.proxy.get_compile_result("false")
        print "Uruchomienie w bezpiecznym watku"
        source_code = """
    import os
    import subprocess
    file_list =  glob.glob("program/*.py")
    i=0;
    i2=0
    for j in file_list:
    	k=os.path.getctime("program/"+j)
    	if k>i:
    		i=k
    		i2="program/"+j;
    subprocess.Popen(i2, stdout=subprocess.PIPE)

    """
        byte_code = compile_restricted(source_code, '<inline>', 'exec')
        print "Uruchomienie w bezpiecznym watku zakonczono"
        p = subprocess.Popen(self.name, stdout=subprocess.PIPE)
        self.result = p.communicate()[0]
        self.proxy.get_program_result(self.result)
        print self.result

    def get_raport(self):
        exist=False
        file_list=glob.glob("/program.*.py")
        for i in file_list:
            if filecmp(i, self.name):
                exist=True;
                break;
        self.proxy.get_program_raport(exist)

clnt=Client()
clnt.main_menu()
clnt.get_program()
clnt.compile_program()
clnt.get_raport()
