#!/usr/bin/python
from SimpleXMLRPCServer import SimpleXMLRPCServer
import xmlrpclib
import threading


class Server(object):
    def __init__(self):
        self.wybor=0
        self.result=None
        self.port=0
        self.ports=list()
        self.server=None
        self.prog='program.py'  # TODO: Zmien to!!!

    def send_program(self):
        handle=open(self.prog)
        return xmlrpclib.Binary(handle.read())
        '''nie zamyka sie pliku zaraz po returnie, z funkcji i tak juz wrocilo, 
           wiec to sie nie wykona, deskryptory i tak posprzata GC lub system'''

    def get_compile_result(self):
        print ("wynik kompilacji: %s" % self.result)
        return self.result

    def get_program_result(self):
        print ("wynik programu: %s" % self.result)
        return self.result

    def get_program_report(self):
        print ("Czy program juz istnieje: %s" % self.result)
        return self.result

    def init_server(self, port):
        # przetestowane, uruchamia sie
        self.server = SimpleXMLRPCServer(("localhost", port))
        return self.server

    def register_functions(self):
        #przetestowane
        try:
            self.server.register_function(self.send_program, 'send_program')
            self.server.register_function(self.get_compile_result, 'get_compile_result')
            self.server.register_function(self.get_program_result, 'get_program_result')
            self.server.register_function(self.get_program_report, 'get_program_report')
        except NameError:
            return False
        else:
            return True

    def run(self, port):
        self.init_server(port)
        print ("Oczekiwanie na polaczenie, port: %s..." % port)
        self.register_functions()
        self.server.serve_forever()  # utrzymanie serwera w petli

    def save_config(self):
            print '''Czy chcesz zapisac porty jako domyslna konfiguracje?
                    1 - TAK
                    2 - NIE'''
            self.wybor = int(raw_input())
            if self.wybor == 1:
                fil = open("konfiguracja_serwera", "w")
                for a in self.ports:
                    fil.write(str(a))
                    fil.write("\n")
                fil.close()
            self.ports = map(int, self.ports)
            return len(self.ports)

    def get_ports(self):
        print "Wpisz porty oddzielajac spacjami"
        self.ports = raw_input().split()
        self.wybor = 0
        return self.ports

    def load_ports(self):
        fil = open("konfiguracja_serwera", "r")
        port2 = fil.readlines()
        fil.close()
        for c in range(len(port2)):
              self.ports.append(port2[c].rstrip())
        self.ports = map(int, self.ports)

    def main_menu(self):
        print "Witaj w programie serwera"
        while self.wybor != 1 and self.wybor != 2:
            print '''Aby uruchomic serwer, wybierz jeden z ponizszych krokow:
            1 - Wpisz porty na ktorych na ktorych ma byc uruchomiony
            2 - Wczytaj skonfigurowane porty'''
            self.wybor=int(raw_input())
        if self.wybor==1:
            self.get_ports()
            while self.wybor != 1 and self.wybor != 2:
                self.save_config()
        else:
            self.load_ports()


srvr=Server()
srvr.main_menu()
print srvr.ports
threads = []

for i in srvr.ports:
    t = threading.Thread(target=Server.run,args=(srvr,i))
    threads.append(t)
    t.start()
